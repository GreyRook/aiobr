=========================
Aiohttp Blueprint Routing
=========================


.. image:: https://img.shields.io/pypi/v/aiobr.svg
        :target: https://pypi.python.org/pypi/aiobr

.. image:: https://readthedocs.org/projects/aiobr/badge/?version=latest
        :target: https://aiobr.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status




Decorator based routing for class based blueprints


* Free software: MIT license
* Documentation: https://aiobr.readthedocs.io.


Features
--------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
